<?php
  use Medoo\Medoo;
  error_reporting(E_ALL);

  class Db {
    private static $database = NULL;
    private function __construct() {}
    public static function getInstance() {
      // Initialize
      $database = new Medoo([
        'database_type' => '',
        'database_name' => '',
        'server' => '',
        'username' => '',
        'password' => ''
      ]);

      return $database;
    }
  }
?>
