<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<?php if (ENV == "development"): ?>
			<script src="http://localhost:35729/livereload.js"></script>
			<meta http-equiv="cache-control" content="no-cache, must-revalidate" />
			<meta http-equiv="pragma" content="no-cache" />
		<?php endif; ?>

		<?php if ($controller == "checkout"): ?>
			<link href="<?php echo CSS ?>checkout.css" rel="stylesheet" type="text/css" media="all" />
		<?php else: ?>
			<link href="<?php echo CSS ?>application.css" rel="stylesheet" type="text/css" media="all" />
		<?php endif; ?>


		<!--[if IE]>
			<link href="assets/css/ie.css" rel="stylesheet" type="text/css" media="all" />
		<![endif]-->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<title>
		</title>

		<link rel="shortcut icon" href="<?php echo IMG ?>favicon.ico" type="image/x-icon" />

		<meta name="author" content="" />
		<meta name="description" content="" />
  	<meta name="keywords" content="" />

		<link rel="canonical" href="" />
		<meta property="og:title" content="" />
		<meta property="og:type" content="" />
		<meta property="fb:app_id" content="">
		<meta property="og:url" content="/" />
		<meta property="og:image" content="">
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content="" />

		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="">
		<meta name="twitter:creator" content="">
		<meta name="twitter:title" content="">
		<meta name="twitter:description" content="">
		<meta name="twitter:image" content="">

		<meta itemprop="name" content="">
		<meta itemprop="description" content="">
		<meta itemprop="image" content="">

		<?php include('ga.php'); ?>
		<?php include('gtm.php'); ?>

	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
